# Maintainer: Nathan Owens <ndowens @ artixlinux.org> 
pkgname=('zfs-dkms' 'zfs-utils' 'zfs-openrc')
pkgbase=zfs
pkgver=0.8.4
pkgrel=1.1
pkgdesc="ZFS modules and init scripts"
arch=('x86_64')
url="https://github.com/openzfs/zfs"
license=('CDDL')
groups=(zfs)
makedepends=('automake' 'python-setuptools')
depends=('python' 'python-cffi')
provides=("ZFS-MODULE=${pkgver}" "SPL-MODULE=${pkgver}" 'zfs')
source=("https://github.com/openzfs/zfs/releases/download/zfs-${pkgver}/zfs-${pkgver}.tar.gz"{,.asc}
	"zfs-utils.initcpio.zfsencryptssh.install"
	"zfs-utils.initcpio.install"
	"zfs-utils.initcpio.hook")
sha256sums=('2b988f5777976f09d08083f6bebf6e67219c4c4c183c1f33033fb7e5e5eacafb'
            'SKIP'
            '29080a84e5d7e36e63c4412b98646043724621245b36e5288f5fed6914da5b68'
            '29a8a6d76fff01b71ef1990526785405d9c9410bdea417b08b56107210d00b10'
            '78e038f95639c209576e7fa182afd56ac11a695af9ebfa958709839ff1e274ce')
validpgpkeys=('4F3BA9AB6D1F8D683DC2DFB56AD860EED4598027')

prepare() {
	cd "$pkgbase-$pkgver"
	./autogen.sh	
}

build() {
	cd "$pkgbase-$pkgver"
    	./configure --prefix=/usr --sysconfdir=/etc --sbindir=/usr/bin --with-mounthelperdir=/usr/bin \
                --libdir=/usr/lib --datadir=/usr/share --includedir=/usr/include --bindir=/usr/bin \
                --with-udevdir=/usr/lib/udev --libexecdir=/usr/lib \
                --with-config=user --disable-systemd --enable-pyzfs
	make
	./scripts/dkms.mkconf -n "$pkgbase" -v "$pkgver" -f dkms.conf
}

package_zfs-utils() {
	install=zfs-utils.install
	depends=('libeudev')
	conflicts=("zfs-utils" "spl-utils")
	backup=('etc/zfs/zed.d/zed.rc' 'etc/default/zfs')
	provides=("zfs-utils" "spl-utils")

	cd "$pkgbase-$pkgver"
    	make DESTDIR="${pkgdir}" install
    	
	# Remove uneeded files
    	rm -r "${pkgdir}"/etc/init.d
    	rm -r "${pkgdir}"/usr/share/initramfs-tools
    	
	# Autoload the zfs module at boot
    	mkdir -p "${pkgdir}/etc/modules-load.d"
    	printf "%s\n" "zfs" > "${pkgdir}/etc/modules-load.d/zfs.conf"
    	
	# fix permissions
    	chmod 750 ${pkgdir}/etc/sudoers.d
    	chmod 440 ${pkgdir}/etc/sudoers.d/zfs
    	
	# Install the support files
    	install -D -m644 "${srcdir}"/zfs-utils.initcpio.hook "${pkgdir}"/usr/lib/initcpio/hooks/zfs
    	install -D -m644 "${srcdir}"/zfs-utils.initcpio.install "${pkgdir}"/usr/lib/initcpio/install/zfs
    	install -D -m644 "${srcdir}"/zfs-utils.initcpio.zfsencryptssh.install "${pkgdir}"/usr/lib/initcpio/install/zfsencryptssh
	install -D -m644 contrib/bash_completion.d/zfs "${pkgdir}"/usr/share/bash-completion/completions/zfs
}


package_zfs-dkms() {
	depends=("zfs-utils=${pkgver}" 'dkms' 'lsb-release')
	conflicts=('spl-dkms')
	replaces=('spl-dkms')
	
	cd "$pkgbase-$pkgver"

	dkmsdir="${pkgdir}/usr/src/${pkgname%-dkms}-${pkgver}"
	install -d ${dkmsdir}

    	cp -a . ${dkmsdir}
	./scripts/dkms.mkconf -v ${pkgver} -f dkms.conf -n zfs
}

package_zfs-openrc() {
	pkgdesc="${pkgdesc} - openrc"
	depends=('openrc' 'zfs-dkms')	
	arch=('any')	

	cd "$pkgbase-$pkgver"/etc/init.d
		
	for i in zfs-{import,mount,zed,share}; do
		install -Dm755 "$i" "$pkgdir"/etc/init.d/"$i"
		sed -i 's|sbin/openrc-run|usr/bin/openrc-run|g' "$pkgdir"/etc/init.d/"$i"
	done 
}
